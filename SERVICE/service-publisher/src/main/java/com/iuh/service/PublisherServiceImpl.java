package com.iuh.service;

import com.iuh.entity.Publisher;
import com.iuh.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    private PublisherRepository repository;

    public List<Publisher> getAllPublishers() {
        return repository.findAll();
    }

    public Optional<Publisher> getPublisherById(String id) {
        return repository.findById(id);
    }

    public Publisher addPublisher(Publisher publisher) {
        return repository.save(publisher);
    }

    public void deletePublisher(String id) {
        repository.deleteById(id);
    }

    public Publisher updatePublisher(String id, Publisher publisher) {
        publisher.setPublisherId(id);
        return repository.save(publisher);
    }
}
