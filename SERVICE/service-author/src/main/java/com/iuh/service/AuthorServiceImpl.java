package com.iuh.service;

import com.iuh.entity.Author;
import com.iuh.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService{

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public List<Author> getAllAuthor(){
        return authorRepository.findAll();
    }

    @Override
    public Optional<Author> getAuthorById(Long authorId) {
        return authorRepository.findByAuthorId(authorId);
    }

    @Override
    public Author saveAuthor(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public Author updateAuthor(Long authorId, Author authorDetails) {
        Author author = authorRepository.findByAuthorId(authorId)
                .orElseThrow(() -> new RuntimeException("Author not found with id: " + authorId));

        author.setName(authorDetails.getName());
        author.setAddress(authorDetails.getAddress());

        return authorRepository.save(author);
    }

    @Override
    public void deleteAuthor(Long authorId) {
        Author author = authorRepository.findByAuthorId(authorId)
                .orElseThrow(() -> new RuntimeException("Author not found with id: " + authorId));

        authorRepository.delete(author);
    }


}
