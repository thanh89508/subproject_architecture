package com.iuh.service;

import com.iuh.entity.Publisher;

import java.util.List;
import java.util.Optional;

public interface PublisherService {

    List<Publisher> getAllPublishers();

    Optional<Publisher> getPublisherById(String id);

    Publisher addPublisher(Publisher publisher);

    void deletePublisher(String id);

    Publisher updatePublisher(String id, Publisher publisher);
}
