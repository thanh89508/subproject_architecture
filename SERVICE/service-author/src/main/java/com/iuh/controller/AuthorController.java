package com.iuh.controller;

import com.iuh.Config.RestTemplateConfig;
import com.iuh.entity.Author;
import com.iuh.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
    @Autowired
    private AuthorService authorService;
    @Autowired
    private RestTemplateConfig restTemplate;

    @GetMapping
    public List<Author> getAllAuthors() {
        return authorService.getAllAuthor();
    }

    @GetMapping("/{authorId}")
    public ResponseEntity<Author> getAuthorById(@PathVariable Long authorId) {

        Optional<Author> author = authorService.getAuthorById(authorId);
        if (author.isPresent()) {
            return ResponseEntity.ok(author.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public Author saveAuthor(@RequestBody Author author) {
        return authorService.saveAuthor(author);
    }

    @PutMapping("/{authorId}")
    public ResponseEntity<Author> updateAuthor(@PathVariable Long authorId, @RequestBody Author authorDetails) {
        try {
            Author updatedAuthor = authorService.updateAuthor(authorId, authorDetails);
            return ResponseEntity.ok(updatedAuthor);
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{authorId}")
    public ResponseEntity<Void> deleteAuthor(@PathVariable Long authorId) {
        try {
            authorService.deleteAuthor(authorId);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/book")
    public ResponseEntity<Object> getStudentAndProduct() {
        Optional<Author> bookOptional = authorService.getAuthorById(1L);

        if (bookOptional.isPresent()) {
            String bookUrl = "http://localhost:8081/api/book";
            Object book = restTemplate.getForObject(bookUrl, Object.class);

            Map<String, Object> result = new HashMap<>();
            result.put("author", bookOptional.get());
            result.put("book", book);

            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PostMapping("/create")
    public ResponseEntity<Author> createAuthor(@RequestBody Author author){
        try{
            Author saveAuthor = authorService.saveAuthor(author);
            return ResponseEntity.ok(saveAuthor);
        }catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }


}
