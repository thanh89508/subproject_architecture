package com.iuh.service;

import com.iuh.entity.Author;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AuthorService {
    List<Author> getAllAuthor();
    Optional<Author> getAuthorById(Long authorId);
    Author saveAuthor(Author author);
    Author updateAuthor(Long authorId, Author authorUpdate);
    void deleteAuthor(Long authorId);

}
